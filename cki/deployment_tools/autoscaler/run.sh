#!/bin/bash
set -euo pipefail

export START_PYTHON_AUTOSCALER="cki.deployment_tools.autoscaler"
export LOG_NAME="autoscaler"

exec cki_entrypoint.sh
