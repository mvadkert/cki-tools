"""Render templates."""
import argparse
import io
import itertools
import os
import pathlib
import re
import sys

from cki_lib import misc
from cki_lib import session
import jinja2
import yaml

SESSION = session.get_session('cki.deployment_tools.render')


def render_envsubst(template_file):
    """Render the template by replacing env variable references."""
    return re.sub(r'\$(\w+|\{([^}]*)\})',
                  lambda m: os.environ[m.group(2) or m.group(1)],
                  template_file.read())


def global_url(url, binary=False, json=False):
    """Try to get a file via requests."""
    response = SESSION.get(url)
    response.raise_for_status()
    return response.content if binary else response.json() if json else response.text


def filter_env(value):
    """Try to get a variable from env if the value matches ${*}."""
    if not isinstance(value, str):
        return value

    # Try to match ${*} with or without brackets.
    return re.sub(
        r'\$(\w+|\{([^}]*)\})',
        lambda m: os.environ[m.group(2) or m.group(1)],
        value
    )


def _read_data_file(file_path, parse):
    contents = file_path.read_text()
    if parse and file_path.suffix == '.j2':
        with io.StringIO(contents) as template_file:
            contents = render_jinja2(template_file)
    return yaml.safe_load(contents) if parse else contents


def _clean_path(path, parse):
    return re.sub(r'\..*', '', path) if parse else path


def _read_data_dir(dir_path, parse):
    data = {}
    extensions = ['.yml', '.yml.j2', '.yaml', '.yaml.j2', '.json', '.json.j2'] if parse else ['']
    for file_path in itertools.chain(*[dir_path.glob(f'**/*{t}') for t in extensions]):
        if not file_path.is_file():
            continue
        file_parts = file_path.relative_to(dir_path).parts
        sub_data = data
        for part in file_parts[:-1]:
            sub_data = sub_data.setdefault(_clean_path(part, parse), {})
        sub_data[_clean_path(file_parts[-1], parse)] = _read_data_file(file_path, parse)
    return data


def _read_data(path_string, parse):
    path = pathlib.Path(path_string)
    return _read_data_file(path, parse) if path.is_file() else _read_data_dir(path, parse)


def render_jinja2(template_file, *, data_paths=None, raw_data_paths=None, include_path=None):
    """Render the template via jinja2."""
    def load_template(name):
        if name == '__template':
            return template_file.read()
        for path in (include_path or []):
            template_path = pathlib.Path(path) / name
            if template_path.exists():
                return template_path.read_text()
        return None

    data = {}
    for name, path in (data_paths or {}).items():
        data[name] = _read_data(path, parse=True)
    for name, path in (raw_data_paths or {}).items():
        data[name] = _read_data(path, parse=False)

    env = jinja2.Environment(loader=jinja2.FunctionLoader(load_template),
                             undefined=jinja2.StrictUndefined)
    env.filters['env'] = filter_env
    env.filters['is_true'] = misc.strtobool
    env.filters['from_yaml'] = yaml.safe_load
    env.globals['url'] = global_url
    env.globals['env'] = os.environ

    template = env.get_template('__template')
    rendered = template.render(data)
    # jinja2 likes to swallow the trailing newline
    if not rendered.endswith('\n'):
        rendered += '\n'
    return rendered


def main(args):
    """Run the main CLI interface."""
    parser = argparse.ArgumentParser(
        description='Render templates',
    )
    parser.add_argument('template', type=argparse.FileType('r'), nargs='?', default=sys.stdin,
                        help='template file to render')
    parser.add_argument('--data', action=misc.StoreNameValuePair,
                        default={}, metavar='NAME=PATH',
                        help='JSON/YAML data file(s) to expose as NAME')
    parser.add_argument('--raw-data', action=misc.StoreNameValuePair,
                        default={}, metavar='NAME=PATH',
                        help='raw data file(s) to expose as NAME')
    parser.add_argument('--include-path', action='append', default=[],
                        help='path for template includes')
    parser.add_argument('--type', default='jinja2', choices=('jinja2', 'envsubst'),
                        help='template type')
    parser.add_argument('--output', type=argparse.FileType('w'), default=sys.stdout,
                        help='Output path for the rendered file.')
    parsed_args = parser.parse_args(args)

    with parsed_args.template as template_file, parsed_args.output as output_file:
        if parsed_args.type == 'jinja2':
            result = render_jinja2(template_file,
                                   data_paths=parsed_args.data,
                                   raw_data_paths=parsed_args.raw_data,
                                   include_path=parsed_args.include_path)
        elif parsed_args.type == 'envsubst':
            result = render_envsubst(template_file)
        else:
            raise Exception(f'Unknown template type {type}')
        output_file.write(result)


if __name__ == '__main__':
    main(sys.argv[1:])
