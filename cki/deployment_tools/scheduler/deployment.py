"""Common scheduler bot deployment helper class."""
import copy
import json
import os


class Deployment:
    """Base class for the deployment of a schedule."""

    def __init__(self, schedule_config, extra_env_variables, disabled=False):
        """Prepare the deployment of a schedule."""
        self._schedule_config = schedule_config
        self._environ = dict(os.environ)
        self._environ.update(extra_env_variables)
        self._disabled = disabled

    def command_line(self, job_name):
        """Get a single command line array to run the job."""
        job_config = self._schedule_config['jobs'][job_name]
        if 'module' in job_config:
            module = job_config['module']
            if isinstance(module, str):
                module = [module]
            return ['python3', '-m'] + module
        if 'command' in job_config:
            return ['bash', '-c', 'set -o pipefail && ' +
                    job_config['command']]
        raise Exception('Shell command or Python module required')

    def command_lines(self, job_name):
        """Get multiple command strings to run the job."""
        job_config = self._schedule_config['jobs'][job_name]
        if 'module' in job_config:
            return [f'python3 -m {job_config["module"]}']
        if 'command' in job_config:
            return ['set -o pipefail', job_config['command']]
        raise Exception('Shell command or Python module required')

    def job_variables(self, job_name, data_dir='/data'):
        """Get the common env variables from the job configuration."""
        job_config = self._schedule_config['jobs'][job_name]
        result = self._variable_dict(job_config.get('variables', []), True)
        result['SCHEDULER_JOB_NAME'] = job_name
        config = copy.deepcopy(job_config.get('config'))
        self.resolve_variables(config)
        result['SCHEDULER_CONFIG'] = json.dumps(config)
        if data_dir:
            result['SCHEDULER_DATA_DIR'] = data_dir
        return result

    def schedule_secrets(self):
        """Return a dict with the mapped secrets."""
        return self._variable_dict(
            self._schedule_config.get('secrets', []), False)

    def schedule_variables(self):
        """Return a dict with the mapped variables."""
        return self._variable_dict(
            self._schedule_config.get('variables', []), True)

    def _variable_dict(self, variables, allow_inline):
        source_vars = set(Deployment._variable_source(v) for v in variables)
        unknown_vars = source_vars - set((None,)) - set(self._environ)
        if unknown_vars:
            raise Exception('The following required environment variables are '
                            f'undefined: {", ".join(unknown_vars)}')
        return dict(self._variable_item(v, allow_inline)
                    for v in variables)

    def resolve_variables(self, config):
        """Recursively resolve variables in nested dicts and lists."""
        if isinstance(config, dict):
            self._resolve_items(config, config.items())
        elif isinstance(config, list):
            self._resolve_items(config, enumerate(config))

    def _resolve_items(self, config, items):
        for key, value in items:
            self.resolve_variables(config[key])
            if isinstance(value, str) and value.startswith('$'):
                if value.startswith('${'):
                    var_name = value[2:-1]
                else:
                    var_name = value[1:]
                if var_name not in self._environ:
                    raise Exception(
                        f'The {var_name} environment variable is undefined')
                config[key] = self._environ[var_name]

    def _variable_item(self, variable, allow_inline):
        if isinstance(variable, str):
            return variable, self._environ[variable]
        key, value = next(iter(variable.items()))
        value = str(value)
        if value.startswith('${'):
            return (key, self._environ[value[2:-1]])
        if value.startswith('$'):
            return (key, self._environ[value[1:]])
        if not allow_inline:
            raise Exception(f'Inline variables not allowed for secrets: {key}')
        return key, value

    @staticmethod
    def _variable_source(variable):
        if isinstance(variable, str):
            return variable
        value = next(iter(variable.values()))
        value = str(value)
        if value.startswith('${'):
            return value[2:-1]
        if value.startswith('$'):
            return value[1:]
        return None

    def image(self, job_name):
        """Return the container image to use for a job."""
        return (self._schedule_config['jobs'][job_name].get('image') or
                self._schedule_config['image'])
