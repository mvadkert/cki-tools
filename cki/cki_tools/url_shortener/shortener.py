"""Shorten URLs."""
import base64
import hashlib
import io
import json
import os
from urllib import parse

from cki_lib import logger
from cki_lib import misc

from .. import _utils

LOGGER = logger.get_logger(__name__)


def lowercase_headers(headers):
    """Return headers with all keys in lowercase.

    HTTP headers are case-insensitive.
    """
    return {k.lower(): v for k, v in headers.items()}


def check_bearer(headers):
    """Check the bearer token.

    Returns None if the check is successful.
    """
    key = os.environ.get('URL_SHORTENER_TOKEN')
    if not key:
        return (404, 'Permission denied: No token configured')
    try:
        secret = headers["authorization"]
    except KeyError:
        return (403, 'Permission denied: missing bearer token')

    if secret != f'Bearer {key}':
        return (403, 'Permission denied: invalid bearer token')

    return None


def create_short_url(headers=None, body: bytes = None, **_):
    """Process a post request to create a new short URL."""
    headers = lowercase_headers(headers)
    check_result = check_bearer(headers)
    if check_result:
        return check_result

    data = json.loads(body)
    if 'url' not in data:
        return (500, 'Missing url')

    url = data['url'].encode('utf8')
    digest = base64.urlsafe_b64encode(hashlib.sha256(url).digest())
    short_digest = digest[:8].decode('utf8')

    s3bucket = _utils.S3Bucket.from_bucket_string(os.environ['URL_SHORTENER_BUCKET_SPEC'])
    short_url = os.path.join(s3bucket.spec.prefix, short_digest)
    try:
        if misc.is_production():
            LOGGER.info('Creating %s -> %s link', short_url, url)
            s3bucket.bucket.upload_fileobj(io.BytesIO(url),
                                           os.path.join(s3bucket.spec.prefix, short_digest))
        else:
            LOGGER.info('Would create %s -> %s if in production mode', short_url, url)

        return (200, parse.urljoin(os.environ['URL_SHORTENER_URL'], short_digest))
    # pylint: disable=broad-except
    except Exception:
        LOGGER.exception('Exception during upload of long URL')
        return (500, 'Internal error')


def forward_short_url(short_digest=None, **_):
    """Process a get request to forward to the underlying long URL."""
    s3bucket = _utils.S3Bucket.from_bucket_string(os.environ['URL_SHORTENER_BUCKET_SPEC'])
    try:
        data = io.BytesIO()
        s3bucket.bucket.download_fileobj(os.path.join(s3bucket.spec.prefix, short_digest), data)
        url = data.getvalue()
        LOGGER.info('Forwarding %s -> %s', short_digest, url)
        return (301, url)
    # pylint: disable=broad-except
    except Exception:
        LOGGER.exception('Exception during retrieval of long URL')
        return (404, 'URL not found or internal error')
