"""Convert pipeline rc-file to kcidb data without touching the pipeline."""
import argparse
import base64
from datetime import datetime
import hashlib
import json
import lzma
import os
import pathlib
import re

from cached_property import cached_property
from cki_lib.logger import get_logger
from cki_lib.misc import get_env_var_or_raise
from cki_lib.misc import strtobool
from cki_lib.session import get_session
from dateutil.parser import parse as date_parse
from kcidb_io.schema import validate
from kcidb_wrap import v3
from kcidb_wrap.v3 import craft_kcidb_data
from rcdefinition.rc_data import SKTData
from rcdefinition.trigger_variables import TriggerVariables

from cki.cki_tools.get_kernel_headers import KojiWrap
from cki.kcidb import utils

LOGGER = get_logger(__name__)
SESSION = get_session(__name__)
CURRENT_TIME = f'{datetime.utcnow().isoformat()}+00:00'


class KCIDBAdapter:
    # pylint: disable=too-many-instance-attributes,too-many-public-methods
    """Convert pipeline data into kcidb data."""

    @classmethod
    def adjust_pipeline_timestamps(cls, dict_obj):
        """Add pipeline finished_at and duration to dumpfile at path of obj_class type."""
        any_change = False
        # We don't distinguish stages or objects; new data doesn't have the fields we fill in.
        try:
            pdict = dict_obj['misc']['pipeline']
            if pdict['created_at']:
                pdict['finished_at'] = CURRENT_TIME
                pdict['duration'] = (date_parse(CURRENT_TIME) -
                                     date_parse(pdict['created_at'])).total_seconds()
                any_change = True
        except KeyError:
            LOGGER.info('Failed to fill in finished_at & duration')

        return any_change

    def upload_file(self, artifacts_path, file_name, file_content=None,
                    source_path=None):
        """Upload a file, if upload is allowed."""
        if not self.upload:
            LOGGER.info('uploading files is disabled')
            return None

        return utils.upload_file(self.visibility, artifacts_path, file_name,
                                 file_content=file_content, source_path=source_path)

    def __init__(self, args):
        """Create object."""
        self.args = args
        self.upload = args.upload

        # Gitlab jobid and pipelineid, both are mandatory.
        self.jobid = get_env_var_or_raise('CI_JOB_ID')
        self.pipeline_id = get_env_var_or_raise('CI_PIPELINE_ID')

        self.build_id = f'redhat:{self.jobid}'

        self.revisions = []
        self.builds = []
        self.tests = []
        self.kcidb_data = None

        self.path2rcfile = pathlib.Path(get_env_var_or_raise('CI_PROJECT_DIR'), 'rc')

        # Simplify attribute access & navigation: either we load a file
        # successfully or we use empty datastructure.
        self.data = (SKTData.deserialize(self.path2rcfile.read_text())
                     if self.path2rcfile.is_file() else SKTData())

        self.visibility = os.environ.get('artifacts_visibility', 'private')

        # Relative path where to put the files in the storage.
        self.artifacts_path_prefix = os.path.join(get_env_var_or_raise('CI_JOB_YMD'),
                                                  str(self.pipeline_id))

    @property
    def revision_artifacts_path(self):
        """Relative path to revision artifacts."""
        return f'{self.artifacts_path_prefix}/{self.revision_id}'

    @staticmethod
    def load_revision_id_from_revision_path():
        """Load revision file and return revision id, or None if it's not available."""
        path2revision_dump = pathlib.Path(get_env_var_or_raise('KCIDB_DUMPFILE_NAME'))
        if path2revision_dump.is_file():
            blob = json.loads(path2revision_dump.read_text())
            try:
                return blob['revisions'][0]['id']
            except (KeyError, IndexError):
                return None

        return None

    @cached_property
    def revision_id(self):
        """ID of the built revision is computed once and stored/loaded from rc file."""
        revision_id = self.load_revision_id_from_revision_path()
        if revision_id:
            return revision_id

        brew_task_id = os.environ.get('brew_task_id') or os.environ.get('copr_build')
        if brew_task_id:
            return hashlib.sha1(str(brew_task_id).encode()).hexdigest()

        # Transform /mbox/ url into /raw/ to get the patch diff only.
        # Patchwork mbox includes headers that can change after people reply to the patches.
        links2raw_patches = [re.sub(r'/mbox/?$', '/raw/', patch_url)
                             for patch_url in os.environ.get('patch_urls', '').split()]
        files_content = self._download_patches(links2raw_patches)
        chash = self.patch_contents2hash(files_content)
        return get_env_var_or_raise('commit_hash') + (f'+{chash}' if chash else '')

    @staticmethod
    def patch_contents2hash(patches_contents):
        """Hash patches to get revision id."""
        hashes = []
        if not patches_contents:
            return None

        for content in patches_contents:
            hashes.append(hashlib.sha256(content).hexdigest())

        hashes = '\n'.join(hashes) + '\n'

        final_hash = hashlib.sha256(hashes.encode('utf-8')).hexdigest()

        return final_hash

    @staticmethod
    def _download_url(url, params=None):
        LOGGER.debug('fetching %s', url)

        params = params if params else {}
        response = SESSION.get(url, stream=True, params=params)
        response.raise_for_status()

        return response.content

    @classmethod
    def _download_patches(cls, patch_links):
        """Download patches using requests + session."""
        files_content = []

        for url in patch_links:
            content = cls._download_url(url)
            files_content.append(content)

        return files_content

    @property
    def build_artifacts_path(self):
        """Get directory of artifacts for this build."""
        return f'{self.artifacts_path_prefix}/build_{self.data.state.kernel_arch}_{self.build_id}'

    @property
    def config_url(self):
        """Return the URL of the build configuration file."""
        if self.data.state.config_file and self.build_artifacts_path:
            return self.upload_file(self.build_artifacts_path, '.config',
                                    source_path=self.data.state.config_file)
        return None

    @property
    def build_log_url(self):
        """Return the URL of the build log file."""
        if self.data.state.buildlog and self.build_artifacts_path:
            return self.upload_file(self.build_artifacts_path, 'build.log',
                                    source_path=self.data.state.buildlog)
        return None

    @property
    def rev_log_url(self):
        """Return the URL of the log file of the attempt to construct revision."""
        if self.data.state.mergelog and self.revision_artifacts_path:
            return self.upload_file(self.revision_artifacts_path, 'merge.log',
                                    source_path=self.data.state.mergelog)
        return None

    @cached_property
    def umb_tests(self):
        """Return UMBTest objects populated with information about tests that ran."""
        # test_job = objects.Job(pipeline, job)
        test_list = json.loads(
            lzma.decompress(
                base64.b64decode(os.environ['data'])
            ).decode()
        )
        umb_tests = []
        for index, test_info in enumerate(test_list):
            test_info['test_index'] = index

            started_at = utils.tzaware_str2utc(os.environ['started_at'])
            misc = {'debug': test_info.get('is_debug', False)}
            misc.update(self.populate_misc(started_at, started_at))

            output_files = []
            for file in test_info['test_log_url']:
                output_files.append({'name': pathlib.Path(file).name, 'url': file})

            sanitize_mapping = {'abort': 'ERROR'}
            try:
                status = sanitize_mapping[test_info['test_result'].lower()]
            except KeyError:
                # Test result not sanitized.
                status = test_info['test_result']

            # This is built in pipeline itself,
            umb_test = v3.UMBTest({'build_id': self.build_id,
                                   'origin': 'redhat',
                                   'id': f'redhat:{self.pipeline_id}_{test_info["test_index"]}',
                                   'path': test_info['test_name'],
                                   'description': test_info['test_description'],
                                   'status': status,
                                   'waived': strtobool(test_info['test_waived']),
                                   'output_files': output_files,
                                   'start_time': started_at,
                                   'misc': misc})
            umb_tests.append(umb_test)
        return umb_tests

    @cached_property
    def selftests(self):
        """Parse selftests into KCIDB tests if present."""
        selftests = []
        path2selftest_results = pathlib.Path(os.environ.get('SELFTESTS_BUILD_RESULTS_PATH', ''))
        start_time = utils.tzaware_str2utc(self.data.build.start_time)
        if path2selftest_results.is_file():
            selftest_data = path2selftest_results.read_text()
            for test_info in utils.parse_selftests(selftest_data, self.data.state.kernel_arch):
                test = v3.Test({
                    'build_id': self.build_id,
                    'origin': 'redhat',
                    'id': f'{self.build_id}_{test_info.get("test_index")}',
                    'path': test_info.get('CKI_UNIVERSAL_ID'),
                    'description': test_info.get('CKI_NAME'),
                    'status': "PASS" if test_info['return_code'] == 0 else "FAIL",
                    'start_time': start_time,
                    'output_files': [],
                    'waived': False,
                    'misc': self.populate_misc(start_time, start_time)
                })
                selftests.append(test)

        return selftests

    def get_brew_revision(self):
        """Return Revision object populated with information about current brew revision."""
        # Use revision (createrepo) start_time.
        # In Brew pipelines, discovery time is always populated.
        discovery_time = utils.tzaware_str2utc(os.environ.get('discovery_time'))
        start_time = utils.tzaware_str2utc(self.data.revision.start_time)

        contact = os.environ.get('submitter') or os.environ.get('owner')
        contacts = [contact] if contact else []
        misc = self.populate_misc(start_time, discovery_time)
        revision = v3.Revision({'id': self.revision_id, 'valid': True, 'origin': 'redhat',
                                'contacts': contacts,
                                'publishing_time': discovery_time,
                                'discovery_time': discovery_time,
                                'tree_name': get_env_var_or_raise('name'),
                                'misc': misc})

        return revision

    def populate_misc(self, started_at, publishing_time):
        """Craft default misc field for kcidb object; specific objects may extend it further."""
        # cki.kcidb.adapter runs in after_script, the build/revision job is done now.
        job_finished_at = f'{datetime.utcnow()}+00:00'.replace(' ', 'T', 1)

        # The pipeline isn't finished, this data must be updated later on.
        pipeline_finished_at = None
        duration = None

        state = self.data.state
        variables = TriggerVariables().pipeline_vars_from_env()

        return {'job': v3.KCIDBObject.job_data(state.tag,
                                               started_at,
                                               job_finished_at,
                                               state.test_hash,
                                               state.commit_message_title,
                                               state.kernel_version),

                'pipeline': v3.KCIDBObject.pipeline_data(variables,
                                                         publishing_time,
                                                         pipeline_finished_at,
                                                         duration)}

    def get_brew_build(self):
        """Return Build object populated with information about current brew build."""
        misc = {'debug': self.is_debug}

        # In Brew pipelines, discovery time is always populated.
        discovery_time = utils.tzaware_str2utc(os.environ.get('discovery_time'))
        start_time = utils.tzaware_str2utc(self.data.build.start_time)
        # Use build (createrepo) start time.
        misc.update(self.populate_misc(start_time, discovery_time))
        brew_id = os.environ.get('brew_task_id') or os.environ.get('copr_build')

        build = v3.Build({
            'origin': 'redhat',
            'revision_id': self.revision_id,
            'architecture': self.data.state.kernel_arch,
            'id': f'redhat:{brew_id}_{self.data.state.kernel_arch}',
            'start_time': start_time,
            'duration': self.duration,
            'valid': bool(self.data.state.kernel_arch),
            'misc': misc
        })

        return build

    @property
    def is_debug(self):
        """Return true if this is a debug kernel."""
        return strtobool(self.data.state.debug_kernel or 'false')

    @cached_property
    def brew_task(self):
        # pylint: disable=no-self-use
        """Get brew task info."""
        koji = KojiWrap(get_env_var_or_raise('web_url'), get_env_var_or_raise('server_url'),
                        get_env_var_or_raise('top_url'))
        return koji.taskinfo(get_env_var_or_raise('brew_task_id'))

    @cached_property
    def copr_task(self):
        """Get COPR task info."""
        if not os.environ.get('copr_build'):
            # Not a COPR task
            return None

        build_id = get_env_var_or_raise('copr_build')
        name = get_env_var_or_raise('name')
        arch = self.data.state.kernel_arch

        copr_server = os.environ.get('COPR_SERVER_URL')
        url = f'{copr_server}/api_2/build_tasks/{build_id}/{name}-{arch}'
        return SESSION.get(url).json()['build_task']

    @property
    def duration(self):
        """Build duration."""
        if os.environ.get('brew_task_id'):
            return (date_parse(self.brew_task['Finished']) -
                    date_parse(self.brew_task['Started'])).total_seconds()

        return self.copr_task['ended_on'] - self.copr_task['started_on']

    def get_revision(self):
        """Add the kernel being tested to builds."""
        # Compute sha256 sums
        # Origin is always redhat
        revision = v3.Revision({'id': self.revision_id, 'origin': 'redhat'})

        # Get some vars from env
        # tree_name: like rhel8, net-next, rdma, mainline ...
        revision.tree_name = get_env_var_or_raise('name')
        # git url of sources
        revision.git_repository_url = get_env_var_or_raise('git_url')
        # This doesn't take into account patches applied on top
        revision.git_commit_hash = get_env_var_or_raise('commit_hash')

        revision.git_commit_name = self.data.revision.git_commit_name

        # git source branch
        revision.git_repository_branch = get_env_var_or_raise('branch')

        # patches applied on top
        revision.patch_mboxes = [{'name': name.replace('/', '-'), 'url': url} for url, name in
                                 zip(self.data.state.patch_data, self.data.state.patch_subjects)]
        revision.message_id = os.environ.get('message_id', None)

        revision.description = self.data.state.patch_subjects[0] if \
            self.data.state.patch_subjects \
            else self.data.revision.description
        revision.publishing_time = \
            utils.tzaware_str2utc(self.data.revision.publishing_time) if \
            self.data.revision.publishing_time else None

        # discovery_time is populated in any pipeline triggered by pipeline-triggers,
        # but not in internal projects, hence this safeguard is necessary.
        discovery_time = os.environ.get('discovery_time')
        revision.discovery_time = utils.tzaware_str2utc(discovery_time) \
            if discovery_time else revision.publishing_time
        # Intentionally left blank for now
        # in the future, determine if kpet-db info should be put here
        revision.contacts = []
        # This determines whether the revision itself it valid, not whether the data in the
        # revision object is valid.
        revision.valid = self.data.state.stage_merge != 'fail'
        start_time = utils.tzaware_str2utc(self.data.revision.start_time)
        revision.misc = self.populate_misc(start_time, revision.publishing_time)

        # Whenever this is None, the element will not be rendered.
        revision.log_url = self.rev_log_url

        return revision

    def get_build(self):
        """Construct build kcidb data."""
        # The revision_id from rc file is used to identify the build, if it's
        # available, otherwise it's computed.
        build = v3.Build({'id': self.build_id, 'origin': 'redhat',
                          'revision_id': self.revision_id})

        with_data = f' with {self.data.state.patch_subjects[0]} patches' \
            if self.data.state.patch_subjects else ''
        build.description = f"CKI build of {get_env_var_or_raise('name')}" + with_data

        build.start_time = utils.tzaware_str2utc(self.data.build.start_time)
        build.duration = self.data.build.duration
        build.architecture = self.data.state.kernel_arch
        build.command = self.data.build.command
        build.compiler = self.data.state.compiler

        build.input_files = []
        build.config_url = self.config_url
        build.output_files = self.build_output_files
        build.config_name = get_env_var_or_raise('config_target')
        # kernel_arch is necessary, if it's missing, we likely didn't build anything valid.
        build.valid = bool(self.data.state.kernel_arch) and (self.data.state.stage_build != 'fail')

        publishing_time = utils.tzaware_str2utc(self.data.revision.publishing_time) \
            if self.data.revision.publishing_time else None
        # Classic Build has start_time saved in rc file.
        build.misc = self.populate_misc(build.start_time, publishing_time)

        # Whenever this is None, the element will not be rendered.
        build.log_url = self.build_log_url

        return build

    @property
    def build_output_files(self):
        """Construct urls to build output files and upload them."""
        files = []
        state = self.data.state
        for fpath in (state.tarball_file, state.selftests_file,
                      state.selftests_buildlog):
            if fpath:
                fpath_obj = pathlib.Path(fpath)
                if not fpath_obj.is_file():
                    LOGGER.error("Job %s: File not found: %s", self.jobid,
                                 fpath)
                    continue
                file_name = fpath_obj.name
                url = self.upload_file(self.build_artifacts_path, file_name,
                                       source_path=str(fpath_obj))
                if url:
                    output_file = {'name': file_name, 'url': url}
                    files.append(output_file)
                    if fpath == state.selftests_buildlog:
                        for selftest in self.selftests:
                            selftest.output_files.append(output_file)

        if self.data.state.repo_path:
            files.append({'name': 'kernel_package_url',
                          'url': self.data.state.kernel_package_url})
        return files

    @staticmethod
    def dump(kcidb_data, to_where):
        """Dump kcidb_data to a file."""
        validate(kcidb_data)

        with open(to_where, 'w') as fhandle:
            json.dump(kcidb_data, fhandle, indent=4)

    @classmethod
    def output_objects(cls, dump_path, objects2dump):
        """Dump kcidb data for kcidb revision/build/test object(s).

        Args:
            dump_path: str, a path including filename where to dump the data
            objects2dump: a list of v3.Revision/Build/Test objects to dump
        """
        # Prepare arrays to pass to craft_kcidb_data.
        revisions, builds, tests = [], [], []

        for object2dump in objects2dump:
            # Check that a single object was passed in args.
            mapping = {v3.Revision: revisions, v3.Build: builds, v3.Test: tests, v3.UMBTest: tests,
                       v3.KSelfTest: tests}
            target = mapping[type(object2dump)]
            dict_item = object2dump.to_mapping()
            # At the end of each job, overwrite pipeline finished_at and duration.
            cls.adjust_pipeline_timestamps(dict_item)

            target.append({key: value for key, value in dict_item.items()
                           if not utils.is_empty(value)})

        # Craft the data.
        kcidb_data = craft_kcidb_data(revisions, builds, tests)
        # Dump the data.
        cls.dump(kcidb_data, dump_path)

    @classmethod
    def find_build_idx(cls, lst, description, revision_id):
        """Search list of kcidb objects for build with matching description and index."""
        for i, item in enumerate(lst):
            if isinstance(item, v3.Build) and item.description == description and \
                    item.revision_id == revision_id:
                return i

        return None

    @classmethod
    def update_base_build(cls, new_objects2dump, old_objects_text):
        """Replace base build with the one from publish stage."""
        # Load a list of old data.
        kcidb_schema = json.loads(old_objects_text)
        old_objects = []
        for key, obj in [('revisions', v3.Revision), ('builds', v3.Build), ('tests', v3.Test)]:
            old_objects += [obj(x) for x in kcidb_schema.get(key)]

        for kcidb_obj in new_objects2dump:
            if isinstance(kcidb_obj, v3.Build):
                # Try to find old base build.
                idx = cls.find_build_idx(old_objects, kcidb_obj.description, kcidb_obj.revision_id)
                if idx is not None:
                    # Delete old base build, kcidb_obj is more-up-to-date.
                    del old_objects[idx]

        return old_objects + new_objects2dump

    @classmethod
    def collect_in_single_dumpfile(cls, path2single_file, new_objects2dump):
        """Load objects from path2single_file and return that data + objects2dump."""
        # Let user know what's happening.
        LOGGER.debug('Trying to update %s', path2single_file)
        if not path2single_file.exists():
            return new_objects2dump

        return cls.update_base_build(new_objects2dump, path2single_file.read_text())


def main():
    """Convert and dump data."""
    parser = argparse.ArgumentParser(
        description='Dump pipeline data as kcidb json.')
    parser.add_argument('artifact',
                        choices=['build', 'revision', 'umb-tests'],
                        help='"build" or "revision". The script will dump'
                             'data into $KCIDB_DUMPFILE_NAME. If $brew_task_id is set, the '
                             'adapter will try to get Brew build/revision.')

    parser.add_argument('--no-upload', default=True, dest='upload',
                        action='store_false',
                        help="By default, files are uploaded. Use --no-upload"
                             "to prevent this behavior.")

    args = parser.parse_args()

    adapter = KCIDBAdapter(args)

    # Is this a brew build/revision?
    is_brew_or_copr = bool(os.environ.get('brew_task_id')) or bool(os.environ.get('copr_build'))
    if args.artifact == 'build':
        objects2dump = [adapter.get_brew_build() if is_brew_or_copr else adapter.get_build()]
        if adapter.selftests:
            objects2dump += adapter.selftests
    elif args.artifact == 'revision':
        objects2dump = [adapter.get_brew_revision() if is_brew_or_copr else adapter.get_revision()]
    elif args.artifact == 'umb-tests':
        objects2dump = adapter.umb_tests
    else:
        raise RuntimeError('invalid cmd-line args!')

    # Load the dumpfile and update it. Everything goes to a single file.
    path2single_file = pathlib.Path(get_env_var_or_raise('KCIDB_DUMPFILE_NAME'))
    path2single_file.parent.mkdir(parents=True, exist_ok=True)

    # Objects are update by now, so we can overwrite the file below.
    updated_objects2dump = adapter.collect_in_single_dumpfile(path2single_file, objects2dump)
    # output_objects is smart enough to put right objects under right keys.
    adapter.output_objects(path2single_file, updated_objects2dump)


if __name__ == '__main__':
    main()
