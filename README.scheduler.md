# `cki.deployment_tools.scheduler` - deploy recurring jobs

## Purpose

The `scheduler` is a deployment helper for recurring jobs that run on a
schedule. Such a schedule can be deployed as an OpenShift `CronJob` or a GitLab
CI/CD pipeline schedule.

## Creating a schedule

The [example schedule](cki/deployment_tools/scheduler/schedule-example.yml)
should give an idea what a schedule looks like.

| Field       | Type   | Required | Description                                                          |
|-------------|--------|----------|----------------------------------------------------------------------|
| `name`      | string | yes      | schedule name, used to build names for deployed resources            |
| `image`     | string | yes      | container image URL including tag                                    |
| `secrets`   | array  | no       | secret [environment variables] to expose to all jobs in the schedule |
| `variables` | array  | no       | [environment variables] to expose to all jobs in the schedule        |
| `jobs`      | dict   | no       | [scheduled jobs]                                                     |

[scheduled jobs]: #jobs
[environment variables]: #environment-variables

### Jobs

The following fields describe the jobs to run:

| Field        | Type         | Required | Description                                                                |
|--------------|--------------|----------|----------------------------------------------------------------------------|
| `schedule`   | string       | yes      | cron-style schedule for the job (e.g. `0 1 * * *`) ([Cron syntax])         |
| `command`    | array        | (yes)    | Bash command line to run (cannot be combined with `module`)                |
| `module`     | string/array | (yes)    | Python module and optional args to run (cannot be combined with `command`) |
| `image`      | string       | no       | container image URL including tag, overrides schedule setting              |
| `variables`  | array        | no       | [environment variables] to expose to this job                              |
| `disabled`   | bool         | no       | ignore this job during deployment                                          |
| `kubernetes` | dict         | no       | [Kubernetes-specific job configuration]                                    |
| `gitlab`     | dict         | no       | [GitLab-specific job configuration]                                        |
| `config`     | dict         | no       | inline yaml configuration, [environment variables] are replaced            |

[Cron syntax]: https://en.wikipedia.org/wiki/Cron
[Kubernetes-specific job configuration]: #kubernetes-specific-job-configuration
[GitLab-specific job configuration]: #gitlab-specific-job-configuration

### Environment variables

Each job can be customized by specifying common secrets and variables, and
per-job variables. Secrets are stored as masked CI/CD variables (GitLab) or
Kubernetes Secrets (OpenShift).

Both are configured as arrays in one of the following formats:

| Item          | Variable name | Variable value                                        |
|---------------|---------------|-------------------------------------------------------|
| - KEY         | `KEY`         | value of `KEY` environment variable during deployment |
| - KEY: $VAR   | `KEY`         | value of `VAR` environment variable during deployment |
| - KEY: INLINE | `KEY`         | inline value of `INLINE` (not supported for secrets)  |

### Kubernetes-specific job configuration

The `kubernetes` field for a job contains configuration only applicable to a
Kubernetes/OpenShift deployment:

| Field                   | Type   | Required | Description                                                                            |
|-------------------------|--------|----------|----------------------------------------------------------------------------------------|
| `memory`                | string | no       | [Kubernetes memory limits and requests]                                                |
| `cpu`                   | float  | no       | [Kubernetes CPU limits and requests], defaults to 1                                    |
| `concurrencyPolicy`     | string | no       | [Kubernetes Concurrency Policy], defaults to `Replace`                                 |
| `activeDeadlineSeconds` | int    | no       | [Kubernetes Init Containers], on the Pod template, defaults to empty                   |
| `serviceAccountName`    | string | no       | [Kubernetes Service Accounts], on the Pod template, defaults to empty                  |
| `dataVolume`            | string | no       | [Kubernetes Volume], `emptyDir` (default), `memory` or name of `persistentVolumeClaim` |
| `subPath`               | string | no       | [Kubernetes Volume sub-path], defaults to empty                                        |
| `labels`                | dict   | no       | [Kubernetes Labels], additional to `app` and `schedule_job`                            |

[Kubernetes memory limits and requests]: https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#meaning-of-memory
[Kubernetes CPU limits and requests]: https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#meaning-of-cpu
[Kubernetes Concurrency Policy]: https://kubernetes.io/docs/tasks/job/automated-tasks-with-cron-jobs/#concurrency-policy
[Kubernetes Init Containers]: https://kubernetes.io/docs/concepts/workloads/pods/init-containers/#detailed-behavior
[Kubernetes Service Accounts]: https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/
[Kubernetes Volume]: https://kubernetes.io/docs/concepts/storage/volumes/
[Kubernetes Volume sub-path]: https://kubernetes.io/docs/concepts/storage/volumes/#using-subpath
[Kubernetes Labels]: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/

### GitLab-specific job configuration

The `gitlab` field for a job contains configuration only applicable to a
GitLab deployment:

| Field  | Type  | Required | Description          |
|--------|-------|----------|----------------------|
| `tags` | array | no       | [GitLab runner tags] |

[GitLab runner tags]: https://docs.gitlab.com/ce/ci/yaml/#tags

## Deploying a schedule

Make sure all secrets in the schedule are defined as environment variables, and
then call

```shell
python3 -m cki.deployment_tools.scheduler -c schedule.yml --environment ENVIRONMENT
```

The following environments are supported:

### OpenShift - `openshift`

Deploys a complete schedule as an OpenShift `CronJob`. All resources are
labeled with `app=scheduler-SCHEDULE` and
`schedule_job=scheduler-SCHEDULE-JOB`. The secrets are deployed as one
common `Secret` resource that is referenced by the individual jobs.

The following additional parameters are supported:

- `--no-cleanup`: optional, do not remove old jobs that are not in the schedule
  anymore
- `--disabled`: optional, disable all jobs independent of the `disabled` settings
  of the schedule

### GitLab - `gitlab`

Deploys a complete schedule as GitLab CI/CD pipeline schedules. The project is
created/reconfigured if needed. The secrets are deployed into masked (if
possible) project variables. The job variables are deployed into schedule
variables. The `.gitlab-ci.yml` CI/CD pipeline configuration is rewritten to
contain one job per schedule.

The following additional parameters are supported:

- `--schedule-project-url`: required, full URL of the GitLab project that will
  hold the CI/CD pipeline schedules
- `--no-cleanup`: optional, do not remove GitLab CI/CD pipeline schedules that
  are not in the schedule anymore

The GitLab personal access token of the owner of the CI/CD pipeline schedules
needs to be provided in the `GITLAB_TOKENS` environment variable:

```shell
export GITLAB_TOKENS='{"gitlab.com": "COM_GITLAB_TOKEN"}'
export COM_GITLAB_TOKEN='0123456789abcdef'
```

### Podman - `podman`

Runs one job of a schedule immediately with the help of Podman.

The following additional parameters are supported:

- `--job JOB`: required, specifies the job to run immediately
- `--code-overlay DIR`: optional, mount a local directory at /code
- `--image IMAGE`: optional, override the image from the schedule

The job is run in a read-only Podman container with a `tmpfs` in `/data`.

### Local shell - `local`

Runs one job of a schedule immediately in the current shell environment.

The following additional parameters are supported:

- `--job JOB`: required, specifies the job to run immediately

## Development

### Environment variables provided to the bots

- `SCHEDULER_CONFIG`: JSON job configuration from the schedule
- `SCHEDULER_DATA_DIR`: writable temporary directory
- `SCHEDULER_JOB_NAME`: name of the scheduled job

### Running a job locally

A job can be run immediately on the local machine in two environments: in the
current shell environment (`local`) or in a Podman container (`podman`).

As an example, this would launch the `git-cache-updater job` of `schedule.yml`
in a Podman container, running code from the current working directory and
using the `cki-tools` image from the local host:

```shell
python3 -m cki.deployment_tools.scheduler -c schedule.yml \
    --environment podman \
    --job git-cache-updater \
    --code-overlay . \
    --image localhost/cki-tools
```

### OpenShift

To manually run a job for a schedule, debug the `CronJob` and run the printed
command manually via

```shell
oc debug cronjob/scheduler-SCHEDULE-JOB
```

To delete everything, run

```shell
oc delete all -l app=scheduler
```
