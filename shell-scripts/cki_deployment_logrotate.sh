#!/bin/bash
# Logrotate configuration for the logs shared NFS
#
# Parameters:
#   - LOGS_PATH: Path where the logs to rotate are stored
#   - OLD_LOGS_DIR: Dir where to put the logs after rotating
#   - STATE_FILE: File to use as lock to prevent duplicate executions
set -euo pipefail

# make getpwuid_r happy
if [ -w '/etc/passwd' ]; then
    echo "cki:x:`id -u`:`id -g`:,,,:${HOME}:/bin/bash" >> /etc/passwd
fi

CONFIG_FILE=$(mktemp)

cat >> "${CONFIG_FILE}" << EOF
"$LOGS_PATH" {
    copytruncate
    daily
    rotate 14
    missingok
    notifempty
    compress
    olddir $OLD_LOGS_DIR
    lastaction
        /usr/bin/find ${OLD_LOGS_DIR} -type f -name "*.gz" -mtime +14 -delete
    endscript
}
EOF

logrotate -fv --state "${STATE_FILE}" "${CONFIG_FILE}"
