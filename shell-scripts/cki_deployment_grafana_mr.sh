#!/bin/bash
set -euo pipefail

# expects the following variables defined in the environment:
# - GRAFANA_{URL,TOKEN}: Grafana credentials
# - GITLAB_PROJECT_URL: git repo without protocol
# - GITLAB_FORK_URL: fork of the git repo without protocol
# - GITLAB_TOKEN: GitLab private token with access to the git repo
# - BRANCH_NAME: branch name in the fork of the git repo
# - GRAFANA_DATA_DIR: directory in the git repo to hold the Grafana data
# - GIT_USER_NAME: Git user name
# - GIT_USER_EMAIL: Git user email

. cki_utils.sh

DATA_PATH="${SCHEDULER_DATA_DIR}/repo"
mkdir -p "${DATA_PATH}"
cd "${DATA_PATH}"

cki_echo_yellow "Fetching repository..."
if ! [ -d .git ]; then
    git clone "https://ignored:$GITLAB_TOKEN@${GITLAB_PROJECT_URL}.git/" .
    cki_echo_green "  successfully cloned main repository"
    git remote add fork "https://ignored:$GITLAB_TOKEN@${GITLAB_FORK_URL}.git/"
    git config user.name "${GIT_USER_NAME}"
    git config user.email "${GIT_USER_EMAIL}"
fi
git fetch --all
cki_echo_green "  successfully fetched updates"

cki_echo_yellow "Checking out '${BRANCH_NAME}'"
if git show-ref --verify --quiet "refs/remotes/fork/${BRANCH_NAME}"; then
    git switch --track "fork/${BRANCH_NAME}"
    git reset --hard "fork/${BRANCH_NAME}"
    cki_echo_green "  switched to branch"
else
    git checkout --track origin/main -b "${BRANCH_NAME}"
    git config "branch.${BRANCH_NAME}.merge" "refs/heads/${BRANCH_NAME}"
    git config "branch.${BRANCH_NAME}.remote" fork
    cki_echo_green "  created branch"
fi

cki_echo_yellow "Downloading from Grafana"
python3 -m cki.deployment_tools.grafana download --path "${GRAFANA_DATA_DIR}"
    cki_echo_green "  completed"

cki_echo_yellow "Checking for changes"
git add "${GRAFANA_DATA_DIR}"
if ! git diff --stat --cached --exit-code; then
    git commit -m "Grafana updates as of $(date +'%F %H:%M')"
    git push \
        --force \
        --push-option merge_request.create \
        --push-option merge_request.title="Grafana updates" \
        --push-option merge_request.remove_source_branch \
        fork
    cki_echo_green "  committed and pushed changes"
else
    cki_echo_green "  no changes found"
fi
