"""Handle get_last_successful_pipeline subcommand."""
import copy

from cki_lib import cki_pipeline


def main(project, args):
    """Handle get_last_successful_pipeline subcommand."""
    variable_filter = copy.copy(args.variable_filter)
    if args.cki_pipeline_type:
        variable_filter['cki_pipeline_type'] = args.cki_pipeline_type
    gl_pipeline = cki_pipeline.last_successful_pipeline_for_branch(
        project, args.cki_pipeline_branch,
        variable_filter=variable_filter)
    return gl_pipeline.id if gl_pipeline else None
