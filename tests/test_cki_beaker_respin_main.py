"""Test beaker respin utility."""
from argparse import Namespace
import contextlib
import io
from pathlib import Path
from subprocess import PIPE
import unittest
from unittest import mock

from bs4 import BeautifulSoup as BS

from cki.beaker_tools.respin.main import BeakerRespin
from cki.beaker_tools.respin.main import MultiRecipeTask
from cki.beaker_tools.respin.main import main

ASSETS_DIR = Path(Path(__file__).absolute().parents[0], 'assets')


class TestBeakerRespin(unittest.TestCase):
    # pylint: disable=no-self-use,too-many-instance-attributes
    """Test beaker respin utility."""

    def setUp(self) -> None:
        """Testcase fixture."""
        # Errors that bkr job-submit catches and retries.
        self.err_strings = ["connection to beaker.engineering.redhat.com failed",
                            "Can't connect to MySQL server on"]

        self.xml = Path(ASSETS_DIR, 'beaker.xml').read_text()
        self.soup = BS(self.xml, 'xml')
        self.recipes = self.soup.find_all('recipe')
        self.recipe0tasks = self.recipes[0].find_all('task')
        self.recipe1tasks = self.recipes[1].find_all('task')
        self.recipe2tasks = self.recipes[2].find_all('task')

        self.std_respin = BeakerRespin(Namespace(**{'dryrun': True, 'jobowner': 'Joe User',
                                                    'repeat': 2, 's': ['Boot test'],
                                                    'mr': 3, 'group': None, 'host': [],
                                                    'samehost': [],
                                                    'noreservesys': False,
                                                    'recipe': [],
                                                    'jobid': 1234,
                                                    'input': 'fake'}),
                                       {'group': {'css_selector': 'job'}})

    @mock.patch('cki.beaker_tools.respin.main.BeakerRespin.get_bkr_results')
    @mock.patch('cki.beaker_tools.respin.main.BeakerRespin.cancel_job')
    def test_conditional_cancel_job(self, mock_cancel, mock_results):
        """Ensure conditional_cancel_job works."""
        whiteboard_true = '<job><whiteboard>respin_job_1234</whiteboard></job>'
        whiteboard_false = '<job><whiteboard>something else</whiteboard></job>'

        mock_results.return_value = whiteboard_false
        with self.assertRaises(RuntimeError):
            BeakerRespin.conditional_cancel_job('J:1234')
        mock_cancel.assert_not_called()

        mock_results.return_value = whiteboard_true
        BeakerRespin.conditional_cancel_job('J:1234')
        mock_cancel.assert_called_with('J:1234')

    @mock.patch('cki.beaker_tools.respin.main.misc.safe_popen')
    def test_cancel_job(self, mock_safe_popen):
        """Ensure cancel_job works."""
        mock_safe_popen.return_value = 'output', 'no error', 0

        BeakerRespin.cancel_job('J:1234')
        mock_safe_popen.assert_called()

        mock_safe_popen.return_value = '', 'some error', 1
        with self.assertRaises(RuntimeError):
            BeakerRespin.cancel_job('J:1234')

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print')
    @mock.patch('time.sleep', lambda *args: None)
    @mock.patch('logging.warning', mock.Mock())
    def test_get_bkr_results(self, mock_print, mock_safe_popen):
        """Ensure get_bkr_results works."""
        mock_safe_popen.return_value = ('some output', '', 1)
        with self.assertRaises(RuntimeError):
            BeakerRespin.get_bkr_results('1234')
            mock_print.assert_called_with('some output')

        mock_safe_popen.return_value = ('some output2', 'Invalid taskspec', 1)
        with self.assertRaises(ValueError):
            BeakerRespin.get_bkr_results('1234')
            mock_print.assert_called_with('some output2')

        mock_safe_popen.return_value = ('', '', 0)
        BeakerRespin.get_bkr_results('1234')

    @mock.patch('cki.beaker_tools.respin.main.misc.retry_safe_popen')
    @mock.patch('cki.beaker_tools.respin.main.LOGGER.info')
    def test_submit_job_no_dryrun(self, mock_info, mock_retry_safe_popen):
        """Ensure submit_job without dryrun raises an exception when no job_id was submitted."""
        args = Namespace(**{'dryrun': False, 'jobowner': None})
        respin = BeakerRespin(args, None)

        mock_retry_safe_popen.return_value = ('', '', 0)
        with self.assertRaises(Exception):
            respin.submit_job('')
            mock_info.assert_any_call('')
            mock_info.assert_any_call('retcode=%s, stderr=%s', 0, '')

    @mock.patch('cki.beaker_tools.respin.main.misc.retry_safe_popen')
    @mock.patch('cki.beaker_tools.respin.main.LOGGER.info')
    def test_submit_job_dryrun(self, mock_info, mock_retry_safe_popen):
        """Ensure submit_job works with dryrun and jobowner."""
        mock_retry_safe_popen.return_value = ('Submitted: [\'J:1234\']', '', 0)
        retval = self.std_respin.submit_job('')

        args2bkr = ["bkr", "job-submit", "--dry-run", '--job-owner=Joe User', '-']
        mock_retry_safe_popen.assert_called_with(self.err_strings, args2bkr, stdin_data=b'',
                                                 stdin=PIPE, stderr=PIPE, stdout=PIPE)
        mock_info.assert_called_with('Submitted job %s (dryrun=%s)', 'J:1234',
                                     self.std_respin.args.dryrun)

        self.assertEqual(retval, 'J:1234')

    @mock.patch('cki.beaker_tools.respin.main.LOGGER.info')
    def test_filter_job_necessary_tasks(self, mock_info):
        """Ensure filter_job_necessary_tasks works."""
        self.std_respin.filter_job_necessary_tasks(self.recipes[2], self.recipe2tasks)

        mock_info('Copying task "%s" %sx', 'taskname2', self.std_respin.args.repeat)

    def test_copy_task(self):
        """Ensure copy_task works."""
        # Create new soup, copy_task method will decompose it!
        recipe = BS('<recipe> </recipe>', 'xml').find('recipe')
        BeakerRespin.copy_task('<task name="Boot test" > <delete_this_elem/>', recipe, 2)
        self.assertEqual(str(recipe), '''<recipe> <task name="Boot test"> </task></recipe>''')

    def test_make_ci_essential_task(self):
        """Ensure make_ci_essential_task works."""
        task = BS('<task name="Boot test" > <params /></task>', 'xml').find('task')
        task2 = BS('<task name="Boot test" > </task>', 'xml').find('task')
        BeakerRespin.make_ci_essential_task(task)
        BeakerRespin.make_ci_essential_task(task2)
        self.assertEqual(str(task), str(task2), '<task name="Boot test"> <params>'
                                                '<param name="CKI_CI_ESSENTIAL" value="True"/>'
                                                '</params> </task>')

    @mock.patch('cki.beaker_tools.respin.main.requests.get')
    def test_get_beaker_xml(self, mock_get):
        """Ensure get_beaker_xml works."""
        jobid = '1234'
        url = f'https://beaker.engineering.redhat.com/to_xml?taskid=J:{jobid}'
        headers = {'Private-Accept': 'application/xml'}

        BeakerRespin.get_beaker_xml(jobid)
        mock_get.assert_called_with(url, headers=headers)

    def test_repeat_manual_tasks(self):
        """Ensure repeat_manual_tasks works."""
        soup = BS('<recipe> <task name="Ignore" /> <task name="Boot test" > <params /> </task>'
                  '</recipe>', 'xml')

        self.std_respin.repeat_manual_tasks(soup)

        # Check that task was copied 3 times.
        self.assertEqual(len(soup.findAll('task', attrs={'name': "Boot test"})),
                         self.std_respin.args.mr)

    def test_destroy_hostrequires(self):
        """Ensure destroy_hostrequires works."""
        recipe = BS(self.xml, 'xml').find('recipe', attrs={'id': 9898})
        expected = '<recipe id="9898" system="oldhost"></recipe>'
        BeakerRespin.destroy_hostrequires(recipe)

        self.assertEqual(str(recipe).replace('\n', ''), expected)

        # Run once again to ensure the method doesn't crash when there's no hostRequires.
        BeakerRespin.destroy_hostrequires(recipe)
        self.assertEqual(str(recipe).replace('\n', ''), expected)

    @mock.patch('cki.beaker_tools.respin.main.LOGGER.info')
    def test_select_host(self, mock_info):
        """Ensure select_host overrides hostRequires correctly for specified recipe_id."""
        soup = BS(self.xml, 'xml')
        recipes = soup.find_all('recipe')
        tested_recipe = soup.find('recipe', attrs={'id': 9898})
        with mock.patch.object(self.std_respin.args, 'host', ['9898:newhost']):
            self.std_respin.select_host(recipes)
            mock_info.assert_called_with('setting host to %s', 'newhost')
            expected = '<recipe id="9898" system="oldhost"><hostRequires force="newhost"/></recipe>'
            self.assertEqual(str(tested_recipe).replace('\n', ''), expected)

    @mock.patch('cki.beaker_tools.respin.main.LOGGER.info')
    def test_select_host_same_host(self, mock_info):
        """Ensure select_host can ensure recipe runs on the same host."""
        soup = BS(self.xml, 'xml')
        recipes = soup.find_all('recipe')
        tested_recipe = soup.find('recipe', attrs={'id': 9898})
        with mock.patch.object(self.std_respin.args, 'samehost', [9898]):
            self.std_respin.select_host(recipes)
            mock_info.assert_called_with('setting host to %s', 'oldhost')
            expected = '<recipe id="9898" system="oldhost"><hostRequires force="oldhost"/></recipe>'
            self.assertEqual(str(tested_recipe).replace('\n', ''), expected)

    def test_partition(self):
        """Ensure partition works."""
        parts = ['a0', 'a1', 'b0', 'b1']
        parts = BeakerRespin.partition(parts, key=lambda x: '0' in x)
        self.assertEqual(parts[True], ['a0', 'b0'])
        self.assertEqual(parts[False], ['a1', 'b1'])

    def test_modify_multirecipe_tasks_no_multirecipe(self):
        """Ensure modify_multirecipe_tasks does not modify XML without multirecipe tasks."""
        soup = BS(self.xml, 'xml')
        self.std_respin.modify_multirecipe_tasks(soup)

        self.assertEqual(soup, BS(self.xml, 'xml'))

    def test_modify_multirecipe_tasks(self):
        """Ensure modify_multirecipe_tasks works for multirecipe tasks."""
        xml = """<recipeSet>
        <recipe id="1234"><task name="taskname1" role="SERVERS" result="Fail"/></recipe>
        <recipe id="5678"><task name="taskname1" role="CLIENTS" result="Fail"/></recipe>
        </recipeSet>"""
        soup = BS(xml, 'xml')
        self.std_respin.modify_multirecipe_tasks(soup)

        # Check that tasks were copied.
        self.assertEqual(len(soup.find_all('task', attrs={'name': 'taskname1'})), 4)

    def test_modify_specified_args(self):
        """Ensure modify_specified_args works to allow dynamic modification."""
        soup = BS(self.xml, 'xml')
        with mock.patch.object(self.std_respin, 'extra_args', {'group': {'css_selector': 'job',
                                                                         'attr_name': 'group'}}):
            with mock.patch.object(self.std_respin.args, 'group', 'newgroup'):
                # Check that group was overriden using "smart" css_selector specification.
                self.std_respin.modify_specified_args(soup)
                self.assertIsNotNone(soup.find('job', attrs={'group': 'newgroup'}))

        with mock.patch.object(self.std_respin.args, 'group', 'newgroup'):
            # Check that group was deleted; extra_args uses css_selector without attr_name.
            self.std_respin.modify_specified_args(soup)
            self.assertIsNone(soup.find('job', attrs={'group': 'a'}))

    @mock.patch('cki.beaker_tools.respin.main.pathlib.Path')
    def test_read_input(self, mock_path):
        """Ensure read_input works for both cases."""
        expected = '<?xml version="1.0" encoding="utf-8"?>\n<job/>'
        with mock.patch.object(self.std_respin, 'get_beaker_xml') as mock_get:
            mock_get.return_value = "<job/>"
            self.assertEqual(expected, str(self.std_respin.read_input()))
            mock_get.assert_called_with(self.std_respin.args.jobid)

        with mock.patch.object(self.std_respin.args, 'jobid', ''):
            mock_path.return_value.read_text.return_value = '<job/>'
            self.assertEqual(expected, str(self.std_respin.read_input()))
            mock_path.assert_called_with(self.std_respin.args.input)

    @mock.patch('cki.beaker_tools.respin.main.pathlib.Path')
    def test_create_output_artifact(self, mock_path):
        """Ensure create_output_artifact works."""
        respin = BeakerRespin(Namespace(**{'output': 'file2write', 'stdout': False}), None)
        respin.create_output_artifact('data')
        mock_path.return_value.write_text.assert_called()


class TestMain(unittest.TestCase):
    """Testcases for main function of main module."""

    def setUp(self) -> None:
        """Testcase fixture."""
        self.mock_read_input = mock.patch('cki.beaker_tools.respin.main.BeakerRespin.read_input',
                                          lambda *a: BS(Path(ASSETS_DIR, 'beaker.xml').read_text(),
                                                        'xml'))
        self.mock_read_input.start()

    @mock.patch('cki.beaker_tools.respin.main.LOGGER.info')
    @mock.patch('cki.beaker_tools.respin.main.BeakerRespin.submit_job')
    def test_main(self, mock_submit_job, mock_info):
        """Ensure main function of main module works with --submit."""
        mock_submit_job.return_value = 'J:1234'
        result = main(['-j', '1234', '--submit'])
        mock_info.assert_any_call('Original job whiteboard is: "%s"', 'testjob')
        mock_submit_job.assert_called()
        self.assertEqual(result, mock_submit_job.return_value)

    @mock.patch('cki.beaker_tools.respin.main.LOGGER.info')
    @mock.patch('cki.beaker_tools.respin.main.BeakerRespin.submit_job')
    def test_main_no_submit(self, mock_submit_job, mock_info):
        """Ensure main function of main module works on dryrun."""
        result = main(['-j', '1234', '-r', '5678'])
        mock_info.assert_any_call('Original job whiteboard is: "%s"', 'testjob')
        mock_submit_job.assert_not_called()
        self.assertIsNone(result)

    @mock.patch('cki.beaker_tools.respin.main.LOGGER.info', mock.Mock())
    @mock.patch('cki.beaker_tools.respin.main.BeakerRespin.submit_job', mock.Mock())
    def test_main_reservesys_present(self):
        """Ensure main function of main module works and puts reservesys into xml."""
        captured_stdout = io.StringIO()
        with contextlib.redirect_stdout(captured_stdout):
            main(['-j', '1234', '-r', '5678', '--stdout'])  # , '--noreservesys'])

        captured_stdout.seek(0)
        self.assertIn('/distribution/reservesys', captured_stdout.read())

    @mock.patch('cki.beaker_tools.respin.main.LOGGER.info', mock.Mock())
    @mock.patch('cki.beaker_tools.respin.main.BeakerRespin.submit_job', mock.Mock())
    def test_main_reservesys_not_present(self):
        """Ensure main function works and --noreservesys does not put reservesys in xml."""
        captured_stdout = io.StringIO()
        with contextlib.redirect_stdout(captured_stdout):
            main(['-j', '1234', '-r', '5678', '--stdout', '--noreservesys'])

        captured_stdout.seek(0)
        self.assertNotIn('/distribution/reservesys', captured_stdout.read())


class TestMultiRecipeTask(unittest.TestCase):
    """Testcases for MultiRecipeTask class."""

    def setUp(self) -> None:
        """Testcase fixture."""
        self.xml = Path(ASSETS_DIR, 'beaker.xml').read_text()
        self.recipes = BS(self.xml, 'xml').find_all('recipe')
        self.recipe0tasks = self.recipes[0].find_all('task')
        self.recipe1tasks = self.recipes[1].find_all('task')
        self.recipe2tasks = self.recipes[2].find_all('task')

    def test_init(self):
        """Test that init pulls right data from recipe and task."""
        # With CKI_SUITE_DESCRIPTION set:
        multirecipetask = MultiRecipeTask(self.recipes[0], self.recipe0tasks[0], None)
        self.assertEqual(multirecipetask.descr, 'A1')

        # Without CKI_SUITE_DESCRIPTION set:
        multirecipetask = MultiRecipeTask(self.recipes[0], self.recipe0tasks[1], None)
        self.assertEqual(multirecipetask.descr, 'taskname2')

    def test_has_failed_tasks_true(self):
        """Ensure has_failed_tasks works; some tasks in the list have failed."""
        result = MultiRecipeTask.has_failed_tasks(
            [MultiRecipeTask(self.recipes[0], self.recipe0tasks[0], None),
             MultiRecipeTask(self.recipes[0], self.recipe0tasks[1], None)])
        self.assertTrue(result)

    def test_has_failed_tasks_false(self):
        """Ensure has_failed_tasks works; all tasks in the list are OK."""
        result = MultiRecipeTask.has_failed_tasks(
            [MultiRecipeTask(self.recipes[0], self.recipe0tasks[0], None),
             MultiRecipeTask(self.recipes[0], self.recipe0tasks[0], None)])
        self.assertFalse(result)

    def test_is_complementary_role_list(self):
        """Ensure is_complementary_role_list works; the list has CLIENTS/SERVERS tasks->success."""
        lst = [MultiRecipeTask(self.recipes[0], self.recipe0tasks[0], None),
               MultiRecipeTask(self.recipes[1], self.recipe1tasks[0], None)]
        self.assertTrue(MultiRecipeTask.is_complementary_role_list(lst))

    def test_is_complementary_role_list_fail_servers(self):
        """Ensure is_complementary_role_list works; no CLIENTS/SERVERS pairs of tasks->fail."""
        lst = [MultiRecipeTask(self.recipes[0], self.recipe0tasks[0], None),
               MultiRecipeTask(self.recipes[2], self.recipe2tasks[0], None)]
        self.assertFalse(MultiRecipeTask.is_complementary_role_list(lst))

    def test_is_complementary_role_list_fail_clients(self):
        """Ensure is_complementary_role_list works; the last has only CLIENTS tasks->fail."""
        lst = [MultiRecipeTask(self.recipes[1], self.recipe1tasks[0], None),
               MultiRecipeTask(self.recipes[1], self.recipe1tasks[0], None)]
        self.assertFalse(MultiRecipeTask.is_complementary_role_list(lst))

    def test_str(self):
        """Ensure __str__ works as expected."""
        result = MultiRecipeTask(self.recipes[1], self.recipe1tasks[0], 1)

        self.assertEqual(str(result), 'taskname1[1]: 5678 CLIENTS')
        self.assertEqual(result.__repr__(), 'taskname1[1]: 5678 CLIENTS')
