"""Gitlab api interaction tests."""
import os
import unittest
from unittest import mock

from cki.cki_tools import _utils
from cki.cki_tools.url_shortener import flask

SECRET = 'secret'


@mock.patch.dict(os.environ, {'URL_SHORTENER_TOKEN': SECRET,
                              'URL_SHORTENER_URL': 'https://shortener.url',
                              'URL_SHORTENER_BUCKET_SPEC': 'minio|user|password|bucket|prefix/'})
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
class TestUrlShortener(unittest.TestCase):
    """ Test URL shortener."""

    def setUp(self):
        """SetUp class."""
        self.client = flask.app.test_client()

    def _post(self, *, path=None, json=None, headers=None):
        path = '/' if path is None else path
        json = {'url': 'https://long'}if json is None else json
        headers = {'Authorization': f'Bearer {SECRET}'}if headers is None else headers
        return self.client.post(path=path, json=json, headers=headers)

    def _get(self):
        return self.client.get('/i02NAqeR')

    def test_entrypoint_missing(self):
        """Check a missing entrypoint."""
        response = self._post(path='/non-existent/url')
        self.assertEqual(response.status, '404 NOT FOUND')

    def test_token_missing(self):
        """Check a missing token."""
        response = self._post(headers={})
        self.assertEqual(response.status, '403 FORBIDDEN')

    def test_token_wrong(self):
        """Check a wrong token."""
        response = self._post(headers={'Authorization': 'Bearer bad_secret'})
        self.assertEqual(response.status, '403 FORBIDDEN')

    @mock.patch.dict(os.environ, {'URL_SHORTENER_TOKEN': ''})
    def test_websecret_env_missing(self):
        """Check a missing token env variable."""
        response = self._post()
        self.assertEqual(response.status, '404 NOT FOUND')

    def test_missing_url(self):
        """Check a missing url."""
        response = self._post(json={})
        self.assertEqual(response.status, '500 INTERNAL SERVER ERROR')

    @mock.patch('boto3.Session.resource')
    def test_create_exception(self, resource):
        """Check exceptions during the creation of redirects."""
        resource().Bucket().upload_fileobj.side_effect = Exception('Boom')
        resource.mock_calls = []
        response = self._post()
        self.assertEqual(response.status, '500 INTERNAL SERVER ERROR')

    @mock.patch('boto3.Session.resource')
    def test_create_redirect(self, resource):
        """Check creating a redirect."""
        response = self._post()
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(resource.mock_calls, mock
                         .call('s3', aws_access_key_id='user',
                               aws_secret_access_key='password',
                               endpoint_url='minio',
                               config=mock.ANY)
                         .Bucket('bucket')
                         .upload_fileobj(mock.ANY, 'prefix/i02NAqeR')
                         .call_list())
        self.assertEqual(resource.mock_calls[-1].args[0].getvalue(), b'https://long')
        self.assertEqual(response.data, b'https://shortener.url/i02NAqeR')

    @mock.patch('boto3.Session.resource')
    def test_create_redirect_non_prod(self, resource):
        """Check creating a redirect in non-production mode."""
        with mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False)):
            response = self._post()
        self.assertEqual(response.status, '200 OK')
        resource.assert_not_called()

    @mock.patch('boto3.Session.resource')
    def test_read_redirect(self, resource):
        """Check reading a redirect."""
        def download_mock(_, data):
            data.getvalue = mock.Mock(return_value='https://long')

        resource().Bucket().download_fileobj.side_effect = download_mock
        resource.mock_calls = []
        response = self._get()

        self.assertEqual(response.status, '301 MOVED PERMANENTLY')
        self.assertEqual(response.headers['Location'], 'https://long')
        self.assertEqual(resource.mock_calls, mock
                         .call('s3', aws_access_key_id='user',
                               aws_secret_access_key='password',
                               endpoint_url='minio',
                               config=mock.ANY)
                         .Bucket('bucket')
                         .download_fileobj('prefix/i02NAqeR', mock.ANY)
                         .call_list())

    @mock.patch('boto3.Session.resource')
    def test_read_exception(self, resource):
        """Check exceptions during the reading of redirects."""
        resource().Bucket().download_fileobj.side_effect = Exception('Boom')
        resource.mock_calls = []
        response = self._get()
        self.assertEqual(response.status, '404 NOT FOUND')


@unittest.skipUnless(os.environ.get('MINIO_URL'), 'No minio server configured')
@mock.patch.dict(os.environ, {'URL_SHORTENER_TOKEN': SECRET,
                              'URL_SHORTENER_URL': 'https://shortener.url'})
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
class IntegrationTestUrlShortener(unittest.TestCase):
    # pylint: disable=too-many-public-methods
    """Run integration tests for the url shortener."""

    def setUp(self):
        """Create the bucket."""
        url = os.environ['MINIO_URL']
        user = os.environ.get('MINIO_ACCESS_KEY', 'user')
        password = os.environ.get('MINIO_SECRET_KEY', 'password')

        self.bucket_spec = f'{url}|{user}|{password}|bucket|prefix/'
        self.bucket = _utils.S3Bucket.from_bucket_string(self.bucket_spec).bucket
        self.bucket.create()

        self.client = flask.app.test_client()

    def tearDown(self):
        """Delete the bucket."""
        self.bucket.objects.all().delete()
        self.bucket.delete()

    def _post(self):
        return self.client.post('/', json={'url': 'https://long'},
                                headers={'Authorization': f'Bearer {SECRET}'})

    def _get(self):
        return self.client.get('/i02NAqeR')

    def test_redirect(self):
        """Create a short URL and retrieve it."""
        with mock.patch.dict(os.environ, {'URL_SHORTENER_BUCKET_SPEC': self.bucket_spec}):
            response = self._post()
            self.assertEqual(response.status, '200 OK')
            self.assertEqual(response.data, b'https://shortener.url/i02NAqeR')
            response = self._get()
            self.assertEqual(response.status, '301 MOVED PERMANENTLY')
            self.assertEqual(response.headers['Location'], 'https://long')

    def test_same_url(self):
        """Create a short URL multiple times and retrieve it."""
        with mock.patch.dict(os.environ, {'URL_SHORTENER_BUCKET_SPEC': self.bucket_spec}):
            for _ in range(2):
                response = self._post()
                self.assertEqual(response.status, '200 OK')
                self.assertEqual(response.data, b'https://shortener.url/i02NAqeR')

            response = self._get()
            self.assertEqual(response.status, '301 MOVED PERMANENTLY')
            self.assertEqual(response.headers['Location'], 'https://long')

    def test_not_present(self):
        """Try to retrieve a short URL that was not submitted."""
        with mock.patch.dict(os.environ, {'URL_SHORTENER_BUCKET_SPEC': self.bucket_spec}):
            response = self._get()
            self.assertEqual(response.status, '404 NOT FOUND')
