#!/usr/bin/python3
"""
Check with the Data Warehouse whether all revisions have their reports.

Required environment variables:
- DATAWAREHOUSE_URL
"""
import datetime
import os
import sys

import requests

DATAWAREHOUSE_URL = os.environ['DATAWAREHOUSE_URL']
SESSION = requests.Session()
SESSION.headers.update({'User-Agent': 'reports-checker/monit'})


def main():
    """Check whether all revisions have their reports."""
    since = datetime.datetime.now() - datetime.timedelta(days=2)
    url = f'{DATAWAREHOUSE_URL}/api/1/kcidb/revisions/reports/missing?since={since}'
    response = SESSION.get(url).json()

    revisions = response['results']

    if not revisions:
        print('📨 All revisions have their reports!')
        return 0

    missing = ', '.join(r['id'] for r in revisions)
    print(f'🔥 {len(revisions)} revisions missing report: {missing}')
    return 1


sys.exit(main())
