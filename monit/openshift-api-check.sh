#!/bin/bash

# no pipefail as the exit code should only be determined by the grep
set -eu

# Check the ability to access the OpenShift API
#
# Usage: openshift-api-check.sh SERVER POD USER PREFIX
#
# Required environment variables:
# - ${PREFIX}_KEY
# - ${PREFIX}_PROJECT

SERVER=$1 POD=$2 USER=$3 PREFIX=$4
KEY_NAME=${PREFIX}_KEY
PROJECT_NAME=${PREFIX}_PROJECT

curl --insecure --silent --show-error --header "Authorization: Bearer ${!KEY_NAME}" "$SERVER/apis/user.openshift.io/v1/users/~" | sponge | grep -q "$USER"
curl --insecure --silent --show-error --header "Authorization: Bearer ${!KEY_NAME}" "$SERVER/api/v1/namespaces/${!PROJECT_NAME}/pods" | sponge | grep -q "$POD"
echo "Connections succeeded"
