#!/usr/bin/python3
"""
Check RabbitMQ queues.

Monitor that messages are not getting queued or not acked.
"""
import argparse
import os
import re
import sys

from cki_lib import misc
import requests

PROTOCOL = os.environ.get('RABBITMQ_PROTOCOL', 'http')
HOST = os.environ.get('RABBITMQ_HOST')
PORT = os.environ.get('RABBITMQ_PORT')
USERNAME = os.environ.get('RABBITMQ_USER', 'guest')
PASSWORD = os.environ.get('RABBITMQ_PASS', 'guest')

MAX_QUEUED = misc.get_env_int('MAX_QUEUED', 10)
MAX_UNACKNOWLEDGED = misc.get_env_int('MAX_UNACKNOWLEDGED', 10)


class Server:
    # pylint: disable=too-few-public-methods
    """RabbitMQ server."""

    def __init__(self, protocol, host, port, user, password, **_):
        # pylint: disable=too-many-arguments
        """Create an object."""
        self.protocol = protocol
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.url = f'{protocol}://{user}:{password}@{host}:{port}/api'

    def query(self, endpoint):
        """Query the server."""
        response = requests.get(f'{self.url}/{endpoint}')
        return response.json()


def check_queues(server, max_queued, max_unacknowledged, queue_name=None):
    """Check all queues are ok."""
    errors = []
    for queue in server.query('queues'):
        name = queue['name']

        if queue_name and not re.match(queue_name, name):
            continue

        messages = queue['messages']
        messages_unacknowledged = queue['messages_unacknowledged']

        error = []
        if messages > max_queued:
            error.append(f'Len {messages}/{max_queued}')
        if messages_unacknowledged > max_unacknowledged:
            error.append(f'Unacked {messages_unacknowledged}/{max_unacknowledged}')

        if error:
            errors.append(f'{name}: {", ".join(error)}')

    if errors:
        print(f'📣 {" | ".join(errors)}')
    else:
        print('✅ All queues look fine.')

    sys.exit(len(errors))


def get_args():
    """Get call arguments."""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description='Check the status of queues in rabbitmq.')
    parser.add_argument('--user', type=str, default=USERNAME,
                        help='Username for authentication to the server.')
    parser.add_argument('--password', type=str, default=PASSWORD,
                        help='Password for authentication to the server.')
    parser.add_argument('--protocol', type=str, default=PROTOCOL,
                        help='Server protocol.')
    parser.add_argument('--host', type=str, default=HOST,
                        help='Server address.')
    parser.add_argument('--port', type=int, default=PORT,
                        help='Server port.')
    parser.add_argument('--max-queued', type=int, default=MAX_QUEUED,
                        help='Max messages allowed in a queue.')
    parser.add_argument('--max-unacknowledged', type=int, default=MAX_UNACKNOWLEDGED,
                        help='Max messages unacknowledged in a queue.')
    parser.add_argument('--queue-name',
                        help='Name of the queues that should be checked. Allows regex notation.')
    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()

    check_queues(Server(**args.__dict__), args.max_queued, args.max_unacknowledged,
                 queue_name=args.queue_name)
