# `shell-scripts/cki_deployment_acme.sh`

Update SSL certificates via dns-01 ACME challenges on Route 53, and deploy them via OpenShift routes.

```shell
Usage: cki_deployment_acme.sh [patch|cron]
```

In `cron` mode, the certificates are checked, updated via the certificate
authority if necessary, and deployed by patching OpenShift routes. In `patch`
mode, existing certificates are only deployed, and no certificate authority is
contacted.

Openshift routes that should be managed need to be annotated with
`cki-project.org/acme: certificate-name`. This [certificate name] normally
corresponds to the first domain name in a line of the `ACME_DOMAINS` variable.

```yaml
apiVersion: route.openshift.io/v1
kind: Route
metadata:
  annotations:
    cki-project.org/acme: cki-project.org
```

| Environment variable          | Type   | Required | Description                                                                                                      |
|-------------------------------|--------|----------|------------------------------------------------------------------------------------------------------------------|
| `IS_PRODUCTION`               | bool   | yes      | Contact either the production or staging endpoint of Let's Encrypt certificate authority.                        |
| `ACME_DOMAINS`                | string | yes      | Domain names for the certificates; certificates are issued per line for one or more space-separated domain names |
| `ACME_AWS_ACCESS_KEY_ID`      | string | yes      | Access key of the AWS service account that is used to modify the Route 53 zones                                  |
| `ACME_AWS_SECRET_ACCESS_KEY`  | string | yes      | Secret key of the AWS service account that is used to modify the Route 53 zones                                  |
| `ACME_BUCKET`                 | string | yes      | Deployment-all style bucket specification for the backup of registration data, keys and certificates             |
| `ACME_PASSWORD`               | string | yes      | Password for the encryption of the backup tarballs                                                               |
| `ACME_OPENSHIFT*_KEY`         | string | yes      | Secrets for the OpenShift service accounts to update the routes                                                  |
| `OPENSHIFT*_PROJECT`          | string | yes      | OpenShift API endpoints for the OpenShift service accounts                                                       |
| `OPENSHIFT*_SERVER`           | string | yes      | OpenShift projects for the OpenShift service accounts                                                            |
| `ACME_SSH_*_HOST`             | string | yes      | user@host for the ssh-able hosts where certs should be updated                                                   |
| `ACME_SSH_*_CERTIFICATE_NAME` | string | yes      | certificate name for the ssh-able hosts                                                                          |
| `ACME_SSH_*_PRIVATE_KEY_PATH` | string | yes      | full path to the secret key for the ssh-able hosts                                                               |
| `ACME_SSH_*_CERTIFICATE_PATH` | string | yes      | full path to the full certificate chain for the ssh-able hosts                                                   |
| `ACME_SSH_*_COMMAND`          | string | no       | command to run after certificate updates for the ssh-able hosts                                                  |
| `*_SSH_PRIVATE_KEY`           | string | yes      | Secret key for the ssh-able hosts                                                                                |

The AWS service account needs permissions equivalent to the following IAM policy:

```yaml
Version: '2012-10-17'
Statement:
  - Effect: Allow
    Action:
      - route53:ListHostedZonesByName
    Resource: '*'
  - Effect: Allow
    Action:
      - route53:ChangeResourceRecordSets
      - route53:ListResourceRecordSets
    Resource: arn:aws:route53:::hostedzone/ZONE-ID
```

The OpenShift service accounts need permissions equivalent to the following Role:

```yaml
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: acme-deployer
  labels:
    app: acme-deployer
rules:
  - apiGroups: [route.openshift.io]
    resources: [routes]
    verbs: [get, list, patch]
  - apiGroups: [route.openshift.io]
    resources: [routes/custom-host]
    verbs: [create]
```

[certificate alias]: https://github.com/dehydrated-io/dehydrated/blob/master/docs/domains_txt.md#wildcards
